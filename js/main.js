$(function() {

  var id = 1;

  $('#add').click(function() {
    
    // customer
    //var cmrSname = getLength($('#c-surname'));
    //var cmrName  = getLength($('#c-name')); 
    //var cmrMname = getLength($('#c-middleName')); 
    //var address  = getLength($('#address')); 
    //var order    = getLength($('#order')); 

    var cSname  = $('#c-surname').val().trim();
    var cName   = $('#c-name').val().trim();
    var cMname  = $('#c-middle-name').val().trim();
    var address = $('#address').val().trim();
    var order   = $('#order').val().trim();

    var rcvSname  = $('#r-surname').val().trim();
    var rcvName   = $('#r-name').val().trim();
    var rcvMname  = $('#r-middle-name').val().trim();
    var status    = $('#status').val().trim();

    var btnEdit   = '<button id="edit"></button>';
    var btnRemove = '<button id="remove"></button>';

    // receiver
    //var rcvSname = getLength($('#r-surname'));
    //var rcvName  = getLength($('#r-name')); 
    //var rcvMname = getLength($('#r-middleName')); 
    //var status   = getLength($('#status'));

    var tdId = setInTd(id);
    
    var tdCmrInitials = setInTd(cSname, cName, cMname); 
    var tdAddress     = setInTd(address);
    var tdOrder       = setInTd(order); 

    var tdRcvInitials = setInTd(rcvSname, rcvName, rcvMname);
    var tdStatus      = setInTd(status);

    var tdButtons = setInTd(btnEdit, btnRemove);

    var client = '<tr>' 
                  + tdId + tdCmrInitials +  tdAddress + tdOrder 
                  + tdRcvInitials + tdStatus + tdButtons +
                 '</tr>';

    $('#db tbody').append(client);
    $('#main').trigger('reset');
    id++;

    return false;

    function getLength(elem) {
      return elem.val().trim().length;
    }

    function setInTd(v1, v2, v3) {
      switch(arguments.length) {
        case 1:
          return '<td>' + v1 + '</td>';
          break;
        case 2:
          return '<td>' + v1 +  v2 + '</td>';
          break;
        case 3:
          return '<td>' + v1 + ' ' + v2 + ' ' + v3 + '</td>';
          break;
      }
    }

  });

  $(document).on('click', '#remove', function() {

    if ($('#add').css('display') != 'none') {
      $(this).closest('tr').nextAll().find('td:first').html(function(i, val) {
        var id = +val;
        return --id;
      });

      id--;

      $(this).closest('tr').remove();
    }
  });

  $(document).on('click', '#edit', function() {
    
    var cmrInitials = $(this).closest('tr').children('td:nth-child(2)').text();
    var address     = $(this).closest('tr').children('td:nth-child(3)').text();
    var order       = $(this).closest('tr').children('td:nth-child(4)').text(); 
    var rcvInitials = $(this).closest('tr').children('td:nth-child(5)').text();
    var status      = $(this).closest('tr').children('td:nth-child(6)').text();
    
    $('#c-surname').val(cmrInitials.split(' ')[0]);
    $('#c-name').val(cmrInitials.split(' ')[1]);
    $('#c-middle-name').val(cmrInitials.split(' ')[2]);
    $('#address').val(address);
    $('#order').val(order);
    $('#r-surname').val(rcvInitials.split(' ')[0]);
    $('#r-name').val(rcvInitials.split(' ')[1]);
    $('#r-middle-name').val(rcvInitials.split(' ')[2]);
    $('#status').val(status);

    $('#add').css('display', 'none');
    $('#update').css('display', '');

    $(this).closest('tr').attr('id', 'editOrder');

  });

  $('#update').click(function() {

    var cmrInitials = $('#c-surname').val().trim() + ' ' + $('#c-name').val().trim() + ' ' + $('#c-middle-name').val().trim();
    var rcvInitials = $('#r-surname').val().trim() + ' ' + $('#r-name').val().trim() + ' ' + $('#r-middle-name').val().trim();
    
    $('#editOrder').children('td:nth-child(2)').text(cmrInitials);
    $('#editOrder').children('td:nth-child(3)').text($('#address').val().trim());
    $('#editOrder').children('td:nth-child(4)').text($('#order').val().trim());

    $('#editOrder').children('td:nth-child(5)').text(rcvInitials);
    $('#editOrder').children('td:nth-child(6)').text($('#status').val().trim());

    $('#add').css('display', '');
    $('#update').css('display', 'none');

    $('#editOrder').removeAttr('id');
    $('#main').trigger('reset');

    return false;

  });

});
